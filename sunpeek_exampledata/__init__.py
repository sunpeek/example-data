from pathlib import Path

DEMO_CONFIG_PATH = Path(__file__).with_name('plant_config_FHW_Arcon_South.json')

DEMO_DATA_PATH_2DAYS = Path(__file__).with_name('FHW__array_ArcS__2017-05-01__2017-05-02__1m__UTC.csv')
DEMO_DATA_PATH_1MONTH = Path(__file__).with_name('FHW__array_ArcS__2017-05-01__2017-05-31__1m__UTC.csv')
DEMO_DATA_PATH_1YEAR = Path(__file__).with_name('FHW__array_ArcS__2017-01-01__2017-12-31__1m__UTC.csv')

DEMO_FLUID_RHO_PATH = Path(__file__).with_name('Pekasolar, pdf export, density.csv')
DEMO_FLUID_CP_PATH = Path(__file__).with_name('Pekasolar, pdf export, heat capacity.csv')

