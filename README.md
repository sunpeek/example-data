# SunPeek Demonstration Package
This package contains sample data for demonstrating the features of the [SunPeek](https://pypi.org/project/sunpeek/) 
solar thermal performance assessment package. To install this package with sunpeek, run `pip install sunpeek[demo]`

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This package is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.